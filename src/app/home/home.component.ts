import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  homeHeader: string = "Welcome to NG2 app";
  homeFooter: string = 'Testing HTTP services on Angular 2';

  constructor() { }

  ngOnInit() {

  }

}
