import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../_shared/models/user';
import { UserService } from '../../_shared/services/user.service';

// Import JQuery - already loaded at front...? - to let server not to worry about "$"
// declare var $: any;
// Better to, first, npm install jquery in Angular, and then import for each component!
// That's the idea of Angular2 as a framework! Check it out: http://stackoverflow.com/a/40355691/2816279
import * as $ from 'jquery';

@Component({
    templateUrl: './user-single.component.html',
    styleUrls: ['./user-single.component.css']

})
export class UserSingleComponent implements OnInit, AfterViewInit {

    isLoading: boolean;
    user: User;    // An element, not an array of elements - see Model "User" on user.ts, under /_shared/models/user

    constructor(
        private route: ActivatedRoute,
        private service: UserService
    ) { }

    ngOnInit() {

        // Set public 'loading' to true:
        this.standby();

        // Grab the id user from the URL
        const
            userId: number = this.route.snapshot.params['id']

        ;
        // Use the UserService (user.service.ts, on /_shared/services) to get user details (getUser() method), through the public API
        this.service.getUser(userId)
            .subscribe(
                user => {
                    // console.error(user);
                    this.user = user;
                    // Say the world data is ready:
                    this.ready(user);
                }
            )
        ;

        // As you can see, using JQuery here will only be used upon first objects, loaded into DOM (app.component.html).
        // All the otehrs components will, still, not be loaded into DOM
        // console.log('On "ngOnInit()": a ".jumbotron" JQuery obj, or array of objs: ', $('.jumbotron'));
    }

    // To use JQUery in the full extent of the DOM, you should use lifecycle hook ngAfterViewInit() to assure every obj is loaded:
    ngAfterViewInit() {

        $(document).ready(function () {
            // console.log('On "ngAfterViewInit()": a ".jumbotron" JQuery obj, or array of objs: ', $('.jumbotron'));
            /* it won't work! *
            $('.jumbotron .close-user').on('click', function (evt) {
                console.log('Hi! THis is JQuery functioning inside Angular 2 framework, on an Observable DOM obj !');
            })
            /**/
        });
        // But, remember, this component is a special case since we'll only show it AFTER API data is loaded...
        // So, here, right now, we'll have the spinning wheel AND NOT a user!
    }

    // The LoadingContainer (spinning wheel while we don't get data from the - live! - API) 2 possible states:
    standby() {
        // console.log('BEFORE http call.');
        this.isLoading = true;
    }

    ready(response) {
        // console.log('Data AFTER call:', response);
        this.isLoading = false;

        // Only now you should be able to bind events on full loaded DOM user-single:
        /**
         * BINDINGS:
         */
        $(document).ready(function () {
            // console.log('On "ready(response)": a ".jumbotron" JQuery obj, or array of objs: ', $('.jumbotron'));
            /** WILL WORK! */
            $('.jumbotron .close-user').on('click', function (evt) {
                // console.log('Hi! THis is JQuery functioning inside Angular 2 framework, on an Observable DOM obj !');
                // We will use the browser history, just for testing latest JQuery integration:
                window.history.back();
                /**
                 * We could use native ng2 'router.navigate([url link])'
                 * calling this function on the HTML's close button ng2 also native event "click":
                 *      <a (click) = "goBack()"><< BACK</a>
                 * and in this component 'ngOnInit()'s, after importing "import { ActivatedRoute, Router } from '@angular/router';"
                 * we could have call:
                        goBack(){
                            this.router.navigate(['/users']);
                        }
                */
            });
            /**/

            // Scroll to top, less the navbar header: only one user is being seen!
            $('html, body')
                .animate({
                    scrollTop: 0 + $('header .navbar > .container-fluid').innerHeight()
                }, 800)
            ;
        });
    }
}
