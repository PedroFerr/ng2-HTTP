import { Component, OnInit } from '@angular/core';
import { User } from '../../_shared/models/user';
import { UserService } from '../../_shared/services/user.service';
// Jquery:
import * as $ from 'jquery';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'user-list',
    templateUrl: '../../users/user-list/user-list-component.html',
    styleUrls: ['../../users/user-list/user-list-component.css']

})

export class UserListComponent implements OnInit {

    isLoading: boolean;
    users: User[];

    constructor(private service: UserService) { }

    ngOnInit() {
        // Set public 'loading' to true:
        this.standby();

        // Call the http service:
        this.service.getUsers()
            .subscribe(
                allUsers => {
                    // console.error(allUsers)
                    this.users = allUsers;  // Already formated to our User[] Type!

                    // Say the world data is ready:
                    this.ready(allUsers);
                }
            )
        ;
    }

    // The LoadingContainer (spinning wheel while we don't get data from the - live! - API) 2 possible states:
    standby() {
        // console.log('BEFORE http call.');
        this.isLoading = true;
    }

    ready(response) {
        // console.log('Data AFTER call:', response);
        this.isLoading = false;

        // ee user-single.component for JQuery operations!
        $(document).ready(function () {
            // Scroll to top (might have got sroll down to see last of all users on small viewport)
            $('html, body')
                .animate({
                    scrollTop: 0 // + $('header .navbar > .container-fluid').innerHeight()
                }, 800)
            ;
        });
    }
}
