import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//Import the service to emitt an event (clicking on any route footer link) to Google Analytics:
import { GaEventsService } from './_shared/services/ga-events.service';
//Jquery:
import * as $ from 'jquery';

//Just for not throwing error ('ga': Google Analytics API function, written on HTML)
declare var ga: any;
@Component({
    selector     : 'app-root',
    templateUrl  : './app.component.html',
    styleUrls    : ['./app.component.css']
})

export class AppComponent implements OnInit {

    private thisYear: number = 9999;
    private currentPath: string;

    constructor (
        private route: ActivatedRoute,
        private router: Router,
        public evtTo_GA: GaEventsService
    ) {  }
  
    ngOnInit() {
      
  
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        var d = new Date();
        this.thisYear = d.getFullYear();
        
        //At each "page" (component) call, send analytics to Google EXCEPT if we are on localhost.
        //We need to 'subscribe' in order to get each state, when routing changes:
        this.router.events.forEach((urlNavigated: any) => {
            //this.router.events.subscribe((urlNavigated: any) => {
            /**
            console.log(urlNavigated);        
            /**/
            //This will log 3 events, on the Router: NavigationStart, RoutesRecognized and NavigationEnd. On each one, we can get 'urlNavigated.url'
            
            //The achieved 'url' property of 'this.router' will only be defined at the last one: NavigationEnd. On the other 2 events we have the last 'url' before the current one... 
            /**
            console.log('this.router.url: ', this.router.url);
            /**/
      
            //NB: we could have added 'NavigationEnd' to the Router imports (import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';)
            //And then "if (urlNavigated instanceof NavigationEnd) {...}" instead of comparing 'this.router.url' with 'urlNavigated.url'...
            
            //So we only want the event where the 'final' url is the one we are now:
            this.currentPath = (this.router.url === urlNavigated.url) ? urlNavigated.url : null;
      
            if(this.currentPath && urlNavigated.id > 1) { //Don't include first (route) entry - no clicks/routing yet! It will fire several ngOnInit() calls...
                //console.log('Routed to:' + window.location.host + this.currentPath);
                this.sendAnalytics('ng2/http' +this.currentPath);
            }
    
        });
    
        //For the first entry page (remember, at the server, the app is installed in /http/ - as it's defined on the <base href> HTML5 head tag)
        //console.log('Hello!', window.location.host + window.location.pathname);
        this.sendAnalytics('ng2' + window.location.pathname);
  
    }

    sendAnalytics(dir){

        //console.log( window.location.host + dir);
    
        //If we are on localhost, don't send any analytics to Google:
        if(window.location.host === 'localhost:4200')
            return;
    
        /*
         * 1st - identify and authorize link ID (https://support.google.com/analytics/answer/2558867?hl=en&utm_id=ad)
         */
        ga('create', 'UA-68500501-1', 'auto');
        ga('require', 'linkid');
    
        /*
         * 2nd - send 'pageview's 
         */
        //Eliminate ghost referrals hiting '/' root site (there will be a filter ("Referrer spam") for '/' in ga admin configuration)
        //http://veithen.github.io/2015/01/21/referrer-spam.html
        //console.log('document.location.pathname = ' + document.location.pathname);
        if (document.location.pathname == '/') {
            /*var page = document.location.pathname.replace('/', '/index.html');  ==> THIS actualy changes and SETS the current pathname! You dumb! ;-)
            ga('send', 'pageview', page);*/
            ga('send', 'pageview', 'ng2/http/index.html')
        } else {
            ga('send', 'pageview', dir);
        }
    
        /*
         * 3rd - associate Analytics user ID with the session user_id
         */
        ga('set', '&uid', 'UA-68500501-1');
    }

    footerLinks_aClick(linkName){
        //console.log(linkName + ' footer link was clicked!');
        
        //Send information (Event) to Google Analytics service (GaEventsService):
        this.evtTo_GA.emitEvent(
            'Footer Clique',
            'Change routing',
            'Page/Component «' + linkName+ '»',
            1
        );
        
        //Remember viewports (height!). If user is, here, clicking at the footer, probably it had to scroll down the page... - scroll to top:
        $('html, body')
            .animate({
              scrollTop: 0 // + $('header .navbar > .container-fluid').innerHeight()
            }, 800)
        ;

    }
  
}
