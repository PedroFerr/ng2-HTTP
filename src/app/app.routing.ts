import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserListComponent } from './users/user-list/user-list-component';
import { UserSingleComponent } from "./users/user-single/user-single.component";
import { AboutComponent } from './about/about.component';
import { NotFoundComponent } from './not-found/not-found.component';


export const routes: Routes = [
    {path: '', component: HomeComponent},
    //{path: '', redirectTo: '/users', pathMatch: 'full'},
    {
        path: 'users',
        component: UsersComponent,
        children: [
            {path: '',        component: UserListComponent},
            {path: ':id',     component: UserSingleComponent}
            //{path: 'create',  component: UserCreateComponent},
            //{path: 'id/edit', component: UserEditComponent}
        ]
    },
    {
        path: 'about',
        component: AboutComponent,
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];



export const routing: ModuleWithProviders = RouterModule.forRoot(routes);