import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
// Allow 'http' calls, over web:
import { HttpClientModule } from '@angular/common/http';

// Operators for communications (calls) reponses:
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { LoadingContainer } from './_shared/components/loading.component';

import { UserService } from './_shared/services/user.service';
import { GaEventsService } from './_shared/services/ga-events.service';

import { UsersComponent } from './users/users.component';
import { UserListComponent } from './users/user-list/user-list-component';
import { UserSingleComponent } from './users/user-single/user-single.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
    declarations: [
        AppComponent,
        LoadingContainer,
        UsersComponent,
        UserListComponent,
        UserSingleComponent,
        HomeComponent,
        AboutComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        routing
    ],
    providers: [
        UserService,
        GaEventsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
