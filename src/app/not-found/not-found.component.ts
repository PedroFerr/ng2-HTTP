import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,  //var route will handle the activated route, containing URL switches/params: let someURLparam = this.route.snapshot.params['id'];
    private router:Router           //var router will handle the Router (which route) state
  ) { }

  ngOnInit() {

  }

  goBack(){
    //We could use the browser history:
    window.history.back();

    //Or use the NG2 Router property:
    //this.router.navigate(['/']);

  }

}
