/**
 * Say we want to have different fields, mapped onto this API fields:
 */
export class ApiUser {
    id: number;
    first_name: string;
    last_name: string;
    avatar: string;
}
/**/
export class User {
    id: number;
    name: string;
    userName: string;
    img: string;
}
/**/
// Then 'map' will "translate" the ones upper to the ones here, once received some data (see user.services.ts)
