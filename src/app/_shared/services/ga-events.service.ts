import { Injectable } from '@angular/core';

//Just for not throwing error ('ga': Google Analytics API function, written on HTML)
declare var ga: any;

@Injectable()

export class GaEventsService {
    
    /**
     * emitEvent
     */
    public emitEvent(
        eventCategory: string,
        eventAction: string,
        eventLabel: string,
        eventValue: number
    ) {
        ga('send', 'event', {
            eventCategory: eventCategory,
            eventAction: eventAction,
            eventLabel: eventLabel,
            eventValue: eventValue
        })
        
    }

    constructor(){}

}
