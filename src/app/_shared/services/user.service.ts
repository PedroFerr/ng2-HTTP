import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, retry } from 'rxjs/operators';

import { ApiUser, User } from '../models/user';

@Injectable()

export class UserService {

    private usersAPI_url = 'https://reqres.in/api/users';

    constructor(
        private http: HttpClient
    ) {}

    // Translate the set of fields of the called API into diferent our ones (see model user.ts):
    private formatUser (user): User  {
        return (
            {
                id: user.id,
                name: `${user.first_name} ${user.last_name}`,
                userName: user.first_name,
                img: user.avatar
            }
        );
    };


    // Grab ALL (array of objs) users from https://www.reqres.in/, a live API test website:
    getUsers(): Observable<User[]> {

        return (
            this.http.get<ApiUser[]>(this.usersAPI_url)
            // .map(resp => resp.json().data)
            // Map array of objs API fields to our ones:
            // .map(users => users.map(this.formatUser))    // <= forEach

            // DATA is coming on ['data'] property of each Obj of the rerturned arry by the HttpCliet called Service:
            .map(users => users['data'].map(this.formatUser))
            // NOTICE we RETURN an Observable<User[]>, waiting for an <ApiUser[]> DATA Type from the called service.

            /**
            .subscribe(users => this.users = users
                /**
                data => {
                    console.log(data.json());
                    this.users  = data.json().data;
                }
                /**
            )
            /**/
            // .catch(this.handleError)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError)
            )
        );
    }

    // Get a SINGLE users
    getUser(id: number): Observable<User> {

        return (
            this.http.get<ApiUser>(this.usersAPI_url + '/' + id)
            .map(user => this.formatUser(user['data']))
            .pipe(
                retry(3),
                catchError(this.handleError)
            )
        );
    }

    // Create a user


    // Update a user


    // Delete a user

    /**
     * Handle as many error types (string, obj, JSON, empty return, etc.) messages we could possible get from the API server response
     */
    private handleError(someError: HttpErrorResponse) {
        // Simplest form, but not grabbing all of them:
        // return Observable.throw(someError.json().data || "Generic service ERROR")

        let error_msg: string;

        if (someError.error instanceof ErrorEvent) {

            const
                obj_error = someError.error.message

            ;

            error_msg = `${someError.status} - ${someError.statusText} || ""} ${obj_error}`;

        } else {

            error_msg = someError.message ? someError.message : someError.toString();
        }

        return Observable.throw(error_msg);
    }
}
