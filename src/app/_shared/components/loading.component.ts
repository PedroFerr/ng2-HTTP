import {Component, Input} from '@angular/core';

@Component({
  selector: 'loading-container',
  template: `
        <div class="jumbotron full-page text-center" *ngIf="callStatus">
            <h2>HTTP call in progress!</h2>
            <p>&nbsp;</p>
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            <p>Please wait...</p>
        </div>
  `
})

export class LoadingContainer {
  
    @Input()
    
    callStatus: boolean;
    
    constructor() {}

    //Interesting example (different selector engineering) from: https://webcake.co/a-spinner-component-using-view-encapsulation-in-angular-2/
    
}